variable "bucket_name" {  
  type        = string
  default = "website.example.com-909090"
  description = "Name of the s3 bucket to be created."
} 
variable "region" {  
  type        = string  
  default     = "us-east-1"  
  description = "Default aws region"
}


