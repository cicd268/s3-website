# s3-website

## Name
S3 Static Website

## Description
This is a static website backed by S3. Contains 2 files:
-  index.html
-  webapp/error.html file

The necessary terraform files for S3 website is present in 
-  main.tf
-  provider.tf
-  variables.tf

The terraform state files are stored in S3 & dynamo db - which are created by terraform files in /tf-s3-backend 


## Make changes and Deploy
Any changes to files will trigger pipeline that will perform
-  terraform validate 
-  terraform plan
- terraform apply - for master merge
-  build and test
-  deploy - for master merge
-  post deploy tests - for master merge 

## Access the website
- http://website.example.com-909090.s3-website-us-east-1.amazonaws.com will show the index.html
- http://website.example.com-909090.s3-website-us-east-1.amazonaws.com/a will show error.html