variable "tf_bucket_name" {  
  type        = string
  default = "s3-tf-backend-0909090"
  description = "Name of the s3 bucket to be created for tf backend"
} 

variable "db_tf_backend" {  
  type        = string  
  default     = "db-tf-backend"  
  description = "Default aws region"
}

variable "region" {  
  type        = string  
  default     = "us-east-1"  
  description = "Default aws region"
}


